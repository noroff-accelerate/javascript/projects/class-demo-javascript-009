import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../models/todo.model';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  public todos$: BehaviorSubject<Todo[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient) { }

  getTodos(): void {
    this.http.get<Todo[]>(`${environment.apiBaseUrl}/posts?userId=1`)
      .subscribe((todos: Todo[]) => {
        this.todos$.next(todos);
      });
  }

  getTodosObservable() {
    return this.http.get<Todo[]>(`${environment.apiBaseUrl}/posts?userId=1`)
  }

  getTodosPromise() {
    return this.http.get<Todo[]>(`${environment.apiBaseUrl}/posts?userId=1`).toPromise();
  }

}
