import { Component, OnInit, OnDestroy } from '@angular/core';
import { Todo } from '../../models/todo.model';
import { TodoService } from '../../services/todo.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-todo-list',
    templateUrl: './todo-list.component.html',
    styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit, OnDestroy {
    public todoItems: Todo[] = [];

    private todo$: Subscription;

    constructor(private todoService: TodoService) {
        this.todo$ = this.todoService.todos$.subscribe(todos => {
            this.todoItems = todos;
        });
    }

    ngOnInit() {

        this.todoService.getTodos();

        // this.todoService.getTodos().subscribe((todos: Todo[]) => {
        //     this.todoItems = todos;
        // }, error => {
        //     console.error(error.message);
        // });

        // try {
        //     this.todoItems = await this.todoService.getTodosPromise();
        // } catch (e) {
        //     console.error(e.message);
        // }

    }


    ngOnDestroy(): void {
        this.todo$.unsubscribe();
    }

    handleTodoCompleted(todoId: number): void {
        const index = this.todoItems.findIndex(todo => todo.id === todoId);
        this.todoItems[index].completed = true;
    }

}
