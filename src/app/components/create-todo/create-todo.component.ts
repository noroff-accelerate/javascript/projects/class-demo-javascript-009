import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.css']
})
export class CreateTodoComponent implements OnInit {

  public createTodoForm: FormGroup = new FormGroup({
    title: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(80)
    ])
  });


  get title(): AbstractControl {
    return this.createTodoForm.get('title');
  }

  handleCreate() {
    
  }

  constructor() {

  }

  ngOnInit(): void {
  }

}
